<?php
/**
 * backupSurveysCron : Allow to backup survey(s) data via cron or sheduled task
 * Need activate cron system in the server : php yourlimesurveydir/application/commands/console.php plugin cron
 *
 * @author Denis Chenu <denis@sondages.pro>
 * @copyright 2017 Denis Chenu <https://www.sondages.pro>
 * @license AGPL v3
 * @version 0.0.2
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

class backupSurveysCron extends \ls\pluginmanager\PluginBase
{
    protected $storage = 'DbStorage';
    static protected $description = 'Allow to backup survey(s) data via cron';
    static protected $name = 'backupSurveysCron';

    /**
     * @var int debug (for echoing on terminal) 0=>ERROR,1=>INFO, 2=>DEBUG, 3=TRACE
     */
    private $debug= 1;
    /**
     * @var int[] survey to save
     */
    private $aSurveys = array(196567,688734);
    /**
     * @var string complete directory name for saved surveys
     */
    private $dirName = "/home/sondages.pro/tmp/ls-test/";
    /**
     * @var string final file name : use [SID] as survey id and [DATE] for date (Y-m-d), [TIME] for time (H:i) and [DATETIME] fopr iso date/time, file is csv
     */
    private $fileName = "survey_result_[SID]-[DATE]-[TIME]";

    /**
    * Add function to be used in cron event
    * @see parent::init()
    */
    public function init()
    {
        /* Action on cron */
        $this->subscribe('cron','backupSurveys');
    }

    /**
     * The action of export as CSV
     * @return void
     */
    public function backupSurveys()
    {
        $this->_LsCommandFix();
        $this->_setConfigs();
        // Faking a session here. (Thanks Mazi ;) )
        Yii::app()->setComponent('session',array('class'=>'CHttpSession',),true);
        Yii::app()->session->open();
        Yii::app()->session['loginID']=1; 
        Yii::app()->loadHelper('common');
        Yii::app()->loadHelper('admin/exportresults');
        Yii::import('application.helpers.viewHelper');
        $savedCount = 0;
        foreach($this->aSurveys as $iSurvey) {
            if(strval(intval($iSurvey)) !== strval($iSurvey)) {
                $this->_log("Invalid value “$iSurvey” for survey",0);
                continue;
            }
            $oSurvey = \Survey::model()->findByPk($iSurvey);
            if(!$oSurvey) {
                $this->_log("Survey “{$iSurvey}” don't exist",0);
                continue;
            }
            if(!$oSurvey->getIsActive()) {
                $this->_log("Survey “{$iSurvey}” not activated",0);
                continue;
            }
            $savedCount++;
            $language=\Survey::model()->findByPk($iSurvey)->language;
            $aFields=array_keys(createFieldMap($iSurvey,'full',true,false,$language));// Broken here : usage of $_SESSION
            if ($oSurvey->savetimings === "Y") {
                //Append survey timings to the fieldmap array
                $aFields = array_merge($aFields,array_keys(createTimingsFieldMap($iSurvey, 'full',true,false,$language)));
            }
            if(\Survey::model()->hasTokens($iSurvey)) {
                //Append token table to the fieldmap array
                $aTokenFields = array_keys(getTokenFieldsAndNames($iSurvey));
                unset($aTokenFields['token']);
                $aFields = array_merge($aFields,$aTokenFields);
            }
            $this->_log("Survey “{$iSurvey}” :Export ".print_r($aFields,1),3);
            $FormattingOptions = new FormattingOptions();
            $FormattingOptions->responseMinRecord=1;
            $FormattingOptions->responseMaxRecord=SurveyDynamic::model($iSurvey)->getMaxId();
            $FormattingOptions->selectedColumns=$aFields;
            $FormattingOptions->responseCompletionState='all';
            $FormattingOptions->headingFormat='short';
            $FormattingOptions->useEMCode=true;
            $FormattingOptions->answerFormat='long';
            $FormattingOptions->output='file';/* file */
            $oExport=new \ExportSurveyResultsService();
            $filename=$oExport->exportSurvey($iSurvey,$language, 'csv',$FormattingOptions, '');
            $newFileName=str_replace(array("[SID]",'[DATE]','[TIME]','[DATETIME]'),array($iSurvey,date("Y-m-d"),date("H:i"),date("c")),$this->fileName);
            $this->_log("Survey “{$iSurvey}” export save as ".$this->dirName.$newFileName,1);
            rename($filename,$this->dirName.$newFileName);
        }
        if($savedCount) {
            $this->_log("$savedCount Survey(s) exported in ".$this->dirName,1);
        }
    }

    /**
    * log an event
    * @param string $sLog
    * @param int $state
    * retrun @void
    */
    private function _Log($sLog,$state=0){
        // Play with DEBUG : ERROR/LOG/DEBUG
        $sNow=date(DATE_ATOM);
        switch ($state){
            case 0:
                $sLevel='error';
                $sLogLog="[ERROR] $sLog";
                break;
            case 1:
                $sLevel='info';
                $sLogLog="[INFO] $sLog";
                break;
            case 2:
                $sLevel='info';
                $sLogLog="[DEBUG] $sLog";
                break;
            default:
                $sLevel='trace';
                $sLogLog="[TRACE] $sLog";
                break;
        }
        Yii::log($sLog, $sLevel,'application.plugins.backupSurveysCron');
        if($state <= $this->debug || $state==0) {
            echo "[{$sNow}] {$sLogLog}\n";
        }
    }
    /**
     * Set whole LimeSurvey config (not needed in new LS version(when ? 3.0))
     * @return void
     */
    private function _setConfigs()
    {
        if(Yii::app()->getConfig('DBVersion') && is_dir(Yii::app()->getConfig('usertemplaterootdir'))) {
            return;
        }
        $aDefaultConfigs = require(Yii::app()->basePath. DIRECTORY_SEPARATOR . 'config' . DIRECTORY_SEPARATOR . 'config-defaults.php');
        foreach($aDefaultConfigs as $sConfig=>$defaultConfig) {
            Yii::app()->setConfig($sConfig,$defaultConfig);
        }
        // Fix rootdir .....
        if(!is_dir(Yii::app()->getConfig('usertemplaterootdir')))
        {
            $sRootDir=realpath(Yii::app()->basePath. DIRECTORY_SEPARATOR . "..") ;
            Yii::app()->setConfig('rootdir',$sRootDir);
            Yii::app()->setConfig('publicdir',$sRootDir);
            Yii::app()->setConfig('homedir',$sRootDir);
            Yii::app()->setConfig('tempdir',$sRootDir.DIRECTORY_SEPARATOR."tmp");
            Yii::app()->setConfig('imagedir',$sRootDir.DIRECTORY_SEPARATOR."images");
            Yii::app()->setConfig('uploaddir',$sRootDir.DIRECTORY_SEPARATOR."upload");
            Yii::app()->setConfig('standardtemplaterootdir',$sRootDir.DIRECTORY_SEPARATOR."templates");
            Yii::app()->setConfig('usertemplaterootdir',$sRootDir.DIRECTORY_SEPARATOR."upload".DIRECTORY_SEPARATOR."templates");
            Yii::app()->setConfig('styledir',$sRootDir.DIRECTORY_SEPARATOR."styledir");
        }
        if(file_exists(Yii::app()->basePath. DIRECTORY_SEPARATOR . 'config' . DIRECTORY_SEPARATOR . 'config.php'))
        {
            $config = require(Yii::app()->basePath. DIRECTORY_SEPARATOR . 'config' . DIRECTORY_SEPARATOR . 'config.php');
            if(is_array($config['config']) && !empty($config['config']))
            {
                foreach($config['config'] as $key=>$value)
                    Yii::app()->setConfig($key,$value);
            }
        }
        $oSettings=SettingGlobal::model()->findAll();
        if (count($oSettings) > 0) {
            foreach ($oSettings as $oSetting) {
                Yii::app()->setConfig($oSetting->getAttribute('stg_name'), $oSetting->getAttribute('stg_value'));
            }
        }
    }
    /**
     * Fix LimeSurvey command function
     * @todo : find way to control API
     * OR if another plugin already fix it
     */
    private function _LsCommandFix()
    {
        if(!class_exists ("DbStorage", false )) {
            /* Bad autoloading in command */
            include_once(dirname(__FILE__)."/DbStorage.php");
        }
         if(!class_exists ("ClassFactory", false )) {
            // These take care of dynamically creating a class for each token / response table.
            Yii::import('application.helpers.ClassFactory');
            ClassFactory::registerClass('Token_', 'Token');
            ClassFactory::registerClass('Response_', 'Response');
        }
    }

}
